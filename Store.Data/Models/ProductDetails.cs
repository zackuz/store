//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Store.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductDetails
    {
        public string Text { get; set; }
        public int ProductId { get; set; }
        public int DetailsId { get; set; }
        public int Id { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual Details Details { get; set; }
    }
}
