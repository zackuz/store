
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/03/2015 11:36:13
-- Generated from EDMX file: C:\Users\Rabbit\Documents\Visual Studio 2013\Projects\Store.Web\Store.Data\Models\Entities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BRAND_STORE];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ProductProducer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_ProductProducer];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductDepartment_Product]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductDepartment] DROP CONSTRAINT [FK_ProductDepartment_Product];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductDepartment_Department]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductDepartment] DROP CONSTRAINT [FK_ProductDepartment_Department];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductProductDetails]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductDetails] DROP CONSTRAINT [FK_ProductProductDetails];
GO
IF OBJECT_ID(N'[dbo].[FK_DetailsProductDetails]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductDetails] DROP CONSTRAINT [FK_DetailsProductDetails];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductComments]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_ProductComments];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductOrderItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderItems] DROP CONSTRAINT [FK_ProductOrderItem];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderOrderItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderItems] DROP CONSTRAINT [FK_OrderOrderItem];
GO
IF OBJECT_ID(N'[dbo].[FK_ProductPicture]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Pictures] DROP CONSTRAINT [FK_ProductPicture];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Products]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Products];
GO
IF OBJECT_ID(N'[dbo].[Producers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Producers];
GO
IF OBJECT_ID(N'[dbo].[Departments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Departments];
GO
IF OBJECT_ID(N'[dbo].[ProductDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductDetails];
GO
IF OBJECT_ID(N'[dbo].[Details]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Details];
GO
IF OBJECT_ID(N'[dbo].[Comments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comments];
GO
IF OBJECT_ID(N'[dbo].[Pictures]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Pictures];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[OrderItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderItems];
GO
IF OBJECT_ID(N'[dbo].[ProductDepartment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductDepartment];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Products'
CREATE TABLE [dbo].[Products] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Model] nvarchar(max)  NOT NULL,
    [Annotation] nvarchar(max)  NOT NULL,
    [ProducerId] int  NOT NULL,
    [Price] float  NOT NULL,
    [Count] int  NOT NULL
);
GO

-- Creating table 'Producers'
CREATE TABLE [dbo].[Producers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Annotation] nvarchar(max)  NOT NULL,
    [Logo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Departments'
CREATE TABLE [dbo].[Departments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [SortValue] int  NOT NULL
);
GO

-- Creating table 'ProductDetails'
CREATE TABLE [dbo].[ProductDetails] (
    [Text] nvarchar(max)  NOT NULL,
    [ProductId] int  NOT NULL,
    [DetailsId] int  NOT NULL,
    [Id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'Details'
CREATE TABLE [dbo].[Details] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Comments'
CREATE TABLE [dbo].[Comments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [ProductId] int  NOT NULL,
    [Mark] tinyint  NOT NULL,
    [DateTime] datetime  NOT NULL
);
GO

-- Creating table 'Pictures'
CREATE TABLE [dbo].[Pictures] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Mime] nvarchar(max)  NOT NULL,
    [Data] varbinary(max)  NOT NULL,
    [ProductId] int  NOT NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Customer] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NOT NULL,
    [Annotation] nvarchar(max)  NULL,
    [Status] int  NULL
);
GO

-- Creating table 'OrderItems'
CREATE TABLE [dbo].[OrderItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ProductId] int  NOT NULL,
    [OrderId] int  NOT NULL,
    [Count] int  NOT NULL
);
GO

-- Creating table 'ProductDepartment'
CREATE TABLE [dbo].[ProductDepartment] (
    [Product_Id] int  NOT NULL,
    [Department_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [PK_Products]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Producers'
ALTER TABLE [dbo].[Producers]
ADD CONSTRAINT [PK_Producers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Departments'
ALTER TABLE [dbo].[Departments]
ADD CONSTRAINT [PK_Departments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductDetails'
ALTER TABLE [dbo].[ProductDetails]
ADD CONSTRAINT [PK_ProductDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Details'
ALTER TABLE [dbo].[Details]
ADD CONSTRAINT [PK_Details]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [PK_Comments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Pictures'
ALTER TABLE [dbo].[Pictures]
ADD CONSTRAINT [PK_Pictures]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrderItems'
ALTER TABLE [dbo].[OrderItems]
ADD CONSTRAINT [PK_OrderItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Product_Id], [Department_Id] in table 'ProductDepartment'
ALTER TABLE [dbo].[ProductDepartment]
ADD CONSTRAINT [PK_ProductDepartment]
    PRIMARY KEY CLUSTERED ([Product_Id], [Department_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ProducerId] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK_ProductProducer]
    FOREIGN KEY ([ProducerId])
    REFERENCES [dbo].[Producers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProducer'
CREATE INDEX [IX_FK_ProductProducer]
ON [dbo].[Products]
    ([ProducerId]);
GO

-- Creating foreign key on [Product_Id] in table 'ProductDepartment'
ALTER TABLE [dbo].[ProductDepartment]
ADD CONSTRAINT [FK_ProductDepartment_Product]
    FOREIGN KEY ([Product_Id])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Department_Id] in table 'ProductDepartment'
ALTER TABLE [dbo].[ProductDepartment]
ADD CONSTRAINT [FK_ProductDepartment_Department]
    FOREIGN KEY ([Department_Id])
    REFERENCES [dbo].[Departments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductDepartment_Department'
CREATE INDEX [IX_FK_ProductDepartment_Department]
ON [dbo].[ProductDepartment]
    ([Department_Id]);
GO

-- Creating foreign key on [ProductId] in table 'ProductDetails'
ALTER TABLE [dbo].[ProductDetails]
ADD CONSTRAINT [FK_ProductProductDetails]
    FOREIGN KEY ([ProductId])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductProductDetails'
CREATE INDEX [IX_FK_ProductProductDetails]
ON [dbo].[ProductDetails]
    ([ProductId]);
GO

-- Creating foreign key on [DetailsId] in table 'ProductDetails'
ALTER TABLE [dbo].[ProductDetails]
ADD CONSTRAINT [FK_DetailsProductDetails]
    FOREIGN KEY ([DetailsId])
    REFERENCES [dbo].[Details]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DetailsProductDetails'
CREATE INDEX [IX_FK_DetailsProductDetails]
ON [dbo].[ProductDetails]
    ([DetailsId]);
GO

-- Creating foreign key on [ProductId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_ProductComments]
    FOREIGN KEY ([ProductId])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductComments'
CREATE INDEX [IX_FK_ProductComments]
ON [dbo].[Comments]
    ([ProductId]);
GO

-- Creating foreign key on [ProductId] in table 'OrderItems'
ALTER TABLE [dbo].[OrderItems]
ADD CONSTRAINT [FK_ProductOrderItem]
    FOREIGN KEY ([ProductId])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductOrderItem'
CREATE INDEX [IX_FK_ProductOrderItem]
ON [dbo].[OrderItems]
    ([ProductId]);
GO

-- Creating foreign key on [OrderId] in table 'OrderItems'
ALTER TABLE [dbo].[OrderItems]
ADD CONSTRAINT [FK_OrderOrderItem]
    FOREIGN KEY ([OrderId])
    REFERENCES [dbo].[Orders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderOrderItem'
CREATE INDEX [IX_FK_OrderOrderItem]
ON [dbo].[OrderItems]
    ([OrderId]);
GO

-- Creating foreign key on [ProductId] in table 'Pictures'
ALTER TABLE [dbo].[Pictures]
ADD CONSTRAINT [FK_ProductPicture]
    FOREIGN KEY ([ProductId])
    REFERENCES [dbo].[Products]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductPicture'
CREATE INDEX [IX_FK_ProductPicture]
ON [dbo].[Pictures]
    ([ProductId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------