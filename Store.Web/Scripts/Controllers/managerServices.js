﻿//if (angular.isUndefined(storeApp)) var storeApp = angular.module('storeApp', ["ngRoute"]);

function TemplateService(service, url) {
    return {
        getSets: function () {
            return service.qget(url);
        },
        saveSet: function (body) {
            return service.qpost(url, body);
        },
        deleteItem: function (id) {
            return service.qdel(url+ "/" + id);
        }
    };
}

angular.module('storeApp').factory('setService', function ($q, webService) {
    return TemplateService(webService, "api/Set");
})

.factory('producerService', function ($q, webService) {
    return TemplateService(webService, "api/Producer");
})

.factory('detailsService', function ($q, webService) {
    return TemplateService(webService, "api/Detail");
})

.factory('prodService', function ($q, webService) {
    return {
        getProduct: function(id) {
            return webService.qget("api/Product/" + id + "?mode=post");
        },
        gerProducers:function() {
            return webService.qget("api/Producer");
        },
        setProduct: function(body) {
            return webService.qpost("api/Product", body);
        }
    };
})

.factory('prodDetailsService', function ($q, webService) {
    return {
        getSets: function (id) {
            return webService.qget('api/ProductDetail/' + id);
        },
        saveSet: function (body) {
            return webService.qpost("api/ProductDetail", body);
        },
        createSet: function(id, body) {
            return webService.qput("api/ProductDetail/" + id, body);
        },
        deleteItem: function (id) {
            return webService.qdel("api/ProductDetail/" + id);
        },
        getDetails: function() {
            return webService.qget("api/Detail");
        }
    };
});