angular.module('storeApp', ["ngRoute", 'angularFileUpload', 'ngAnimate']);

angular.module('storeApp')

.controller('testController', function($scope, webService) {
    //po - promise object
        var value = { Text: 'ot' };
        var po = webService.qput('api/Search', value);
        po.then(function(data) {
            $scope.data = data;
        }, function(data, status) {
            alert(status);
        });

    })


.controller("navigationController", function ($scope, $rootScope, $q, $location, webService) {
    $rootScope.Text = 'Text';
    var promiseObj = webService.getData();
    promiseObj.then(function (value) {
        insertAll(value);
        $scope.selectedProducer = $scope.producers[0];
        $scope.selectedList = $scope.lists[0];
    });

    //Functions
    $scope.selectList = function (index) {
        promiseObj = webService.getProducers(index);
        promiseObj.then(function (value) {
            insertProducer(value);
            $scope.selectedProducer = $scope.producers[0];
            $scope.selectedList = findById($scope.lists, index);
            $location.path("/list/" + $scope.selectedList.Id +"/0");
        });
    };

    $scope.selectProducer = function (index) {
        $scope.selectedProducer = findById($scope.producers, index);
        if ($scope.selectedList.Id == -1) $location.path("/search/" + $scope.searchText + "/" + $scope.selectedProducer.Id);
        else $location.path("/list/" + $scope.selectedList.Id + "/" + $scope.selectedProducer.Id);
    };

    $scope.search = function() {
        $scope.selectedList = { Id: -1, Name: "Search" };
        $scope.selectedProducer = $scope.producers[0];
        promiseObj = webService.qput('api/Search', { Text: $scope.searchText });
        promiseObj.then(function(data) {
            $scope.producers = getProdusers(data);
        });
        $location.path("/search/" + $scope.searchText + "/" + $scope.selectedProducer.Id);
    }
    //$scope.test = function() {
    //    var promise = webService.test();
    //    promise.then(function(data) {
    //        alert(data[0] + " + " + data[1]);
    //    }, function (msg) { alert(msg); });
    //}
    //Privare functions
    function insertTable(data) {
        $scope.products = data.Products;
        $scope.paging = data.Paging;
    };
    function insertProducer(data) {
        $scope.producers = getProdusers(data.Producers);
        insertTable(data);
    };
    function insertAll(data) {
        $scope.lists = data.Departments;
        insertProducer(data);
    };
    function getProdusers(producers) {
        var produserList = [{ Id: -1, Name: "All producers" }];
        for (var i = 0; i < producers.length; i++) {
            produserList[i + 1] = producers[i];
        }
        return produserList;
    }
    function findById(list, id) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Id == id) return list[i];
        }
        return null;
    }
})

.controller("listController", function ($scope, $location, $routeParams, $q, webService, cartService, compareService) {
    var promiseObj = {};
    if (angular.isUndefined($routeParams.producerId)) $scope.producerId = 0;
    else $scope.producerId = $routeParams.producerId;
    if (angular.isUndefined($routeParams.listId)) {
        if (!angular.isUndefined($routeParams.text))
            promiseObj = webService.getSearchList($routeParams.text, $scope.producerId);
    }
    else {
        $scope.listId = $routeParams.listId;
        promiseObj = webService.getProductList($scope.listId, $scope.producerId);        
    }

    promiseObj.then(function (value) {
        $scope.items = value;
    });

    $scope.goDetails = function (index) {
        $location.path("/item/" + index);
    }

    $scope.addToCart = function (product) {
        cartService.add(product);
    };
    $scope.compare = function(product) {
        compareService.add(product);
    }
})

.controller("productController", function ($scope, $location, $routeParams, $q, webService, cartService) {
    var promiseObj = webService.getProductDetails($routeParams.itemId);
    promiseObj.then(function (value) {
        $scope.product = value;
    });

    $scope.addToCart = function (product) {
        cartService.add(product);
    };

    $scope.showPic = function (index) {
        alert(index);
        $scope.selectPic = $scope.product.picturesIds[index];
    };

    $scope.setMark = function(n) {
        $scope.mark = n;
    };
    $scope.hoverMark = function (n) {
        $scope.hover = n;
    };
    $scope.stopMark = function () {
        $scope.hover = $scope.mark;
    };
    $scope.sendComment = function () {
        var comment = {
            User: $scope.name,
            Text: $scope.text,
            Mark: $scope.mark,
            ProductId: $scope.product.Id
        };
            promiseObj = webService.qpost('api/Comment', comment);
            promiseObj.then(function(data) {
                comment.Id = data;
                comment.DateTime = 'Newly';
                $scope.product.Comments.push(comment);
                $scope.mark = 0;
                $scope.name = $scope.text = '';
            } );
        };
    })

.controller("cartController", function ($scope, $location, cartService) {
    $scope.startOrder = function () {
        var promiseObj = cartService.startOrder($scope.userName, $scope.phone);
        promiseObj.then(function() {
            $location.path("/success/");
        }, function() { alert("error"); });
    }
    $scope.delete = function(index) {
        cartService.remove(index);
    };
    $scope.clearAll = function() {
        cartService.clear();
    };
    $scope.downCount = function(index) {
        cartService.downCount(index);
    }
    $scope.upCount = function (index) {
        cartService.upCount(index);
    }

    $scope.test = function() {
        alert($scope.orderForm.$vailid);
    }
})

.controller('compareController', function ($scope, compareService) {
    var promiseObj = compareService.compare();
        promiseObj.then(function(value) {
            $scope.data = value;
        });
        $scope.delete = function(index) {
            compareService.delete(index, $scope.data);
        };
    })

.controller("usersController", function($scope, adminService) {
    var promiseObj = adminService.getUsers();
    promiseObj.then(function(value) {
        $scope.users = value.Users;
        $scope.roles = value.Roles;
    });

    $scope.addUser = function() {
        promiseObj = adminService.addUser($scope.name, $scope.password);
        promiseObj.then(function(value) {
            $scope.users.push(value);
        });
    };

    $scope.delete = function(index) {
        promiseObj = adminService.deleteUser($scope.users[index].Name);
        promiseObj.then(function(value) {
            $scope.users.splice(index, 1);
        });
    }

    $scope.addRole = function(index, role) {
        promiseObj = adminService.addRole($scope.users[index].Name, role);
        promiseObj.then(function (value) {         
            $scope.users[index].Roles.push(role);
        });
    }

    $scope.deleteUserFromRole = function(index, role) {
        promiseObj = adminService.deleteUserFromRole($scope.users[index].Name, role);
        promiseObj.then(function (value) {
            for (var i = 0; i < $scope.users[index].Roles.length; i++) {
                if ($scope.users[index].Roles[i] == role) {
                    $scope.users[index].Roles.splice(i, 1);
                    break;
                }
            }
        });
    }
});

