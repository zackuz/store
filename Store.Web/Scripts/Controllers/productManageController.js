﻿//if (angular.isUndefined(storeApp)) var storeApp = angular.module('storeApp', ["ngRoute"]);

angular.module('storeApp').controller("productManageController",['$scope', '$rootScope', 'prodService', '$routeParams', function ($scope, $rootScope, prodService, $routeParams) {
    var promiseObj;
    $rootScope.showTab = 'Comments';
    $scope.editMode = false;
    $scope.refresh = function () {
        if ($routeParams.itemId == 0) {
            var product = {
                Id: 0,
                Name: "",
                Model: "",
                Annotation: "",
                Producer: "",
                Price: 0,
                Count: 0
            };
            $scope.product = product;
            $scope.editMode = true;
        }
        promiseObj = prodService.getProduct($routeParams.itemId);
        promiseObj.then(function(value) {
            $scope.product = value;
        });
    }

    $scope.refresh();
    $scope.changeTab = function(tab) {
        $rootScope.showTab = tab;
    };
    $scope.startEdit = function () {
        $scope.tabBuf = angular.copy($rootScope.showTab);
        $rootScope.showTab = 'EditMode';
        $scope.select = angular.copy($scope.product);
        $scope.editMode = true;
        promiseObj = prodService.gerProducers();
        promiseObj.then(function(value) {
            $scope.producers = value;
            for (var i = 0; i < $scope.producers.length; i++) {
                if ($scope.producers[i].Name == $scope.product.Producer) {
                    $scope.selectedProducer = $scope.producers[i];
                    break;
                }
            }
        });
       
    }

    $scope.cancelEdit = function() {
        $rootScope.showTab = $scope.tabBuf;
        $scope.product = $scope.select;
    }

    $scope.save = function () {
        var product = {
            Id: $scope.product.Id,
            Name: $scope.product.Name,
            Model: $scope.product.Model,
            Annotation: $scope.product.Annotation,
            ProducerId: $scope.selectedProducer.Id,
            Price: $scope.product.Price,
            Count: $scope.product.Count
        };
        promiseObj = prodService.setProduct(product);
        promiseObj.then(function () {
            $rootScope.showTab = $scope.tabBuf;
        });
    }

}])


.controller("productDetailsController", function ($scope, prodDetailsService, $routeParams) {
    TemplateScope($scope, prodDetailsService, [
        { key: "Id", scope: "" },
        { key: "Name", scope: "name" },
        { key: "Value", scope: "value" }
    ], $routeParams.itemId);
    var promiseObj;
    $scope.afterRefresh = function() {
        promiseObj = prodDetailsService.getDetails();
        promiseObj.then(function(value) {
            $scope.details = value;
        });
    };
    $scope.create = function() {
        var productDetail = {
            Id: 0,
            DetailId: $scope.selectedDetail.Id,
            Value: $scope.value
        };
        promiseObj = prodDetailsService.createSet($routeParams.itemId, productDetail);
        promiseObj.then(function() {
            $scope.refresh();
        });
    }
    $scope.refresh();

})

.controller("newProductController", function ($scope, webService, prodService, $location) {
    var promiseObj = prodService.gerProducers();
    promiseObj.then(function (value) {
        $scope.producers = value;      
    });

    $scope.create = function () {
        $scope.product.ProducerId = $scope.selectedProducer.Id;
        promiseObj = webService.qpost('api/Product', $scope.product);
        promiseObj.then(function(value) {
            $location.path('/item/' + value);
        },function() {
            alert('Post error. Try again later.');
        });
    }
})

.controller("productDepartmentController", function ($scope, webService, $routeParams) {
    var promiseObj = webService.qget('api/Department/'+$routeParams.itemId);
    promiseObj.then(function (value) {
        $scope.list = value;
    });

    $scope.save = function() {
        promiseObj = webService.qput('api/Department/' + $routeParams.itemId, $scope.list);
        promiseObj.then(function (value) {
            alert('All right. Changes saved.');
        }, function () {
            alert('Post error. Try again later.');
        });
    };
});