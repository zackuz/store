﻿//if (angular.isUndefined(storeApp)) var storeApp = angular.module('storeApp', ["ngRoute"]);

angular.module('storeApp').factory('cartService', function ($q, $rootScope, webService) {
    if (angular.isUndefined($rootScope.CartList))
        $rootScope.CartList = { price: 0, items: null };

    function findById(id) {
        if ($rootScope.CartList.items == null) return null;
        for (var i = 0; i < $rootScope.CartList.items.length; i++) {
            if ($rootScope.CartList.items[i].Product.Id == id) return i;
        }
        return null;
    }
    return {
        add: function (product) {
            var id = findById(product.Id);
            if (id == null) {
                if ($rootScope.CartList.items == null)
                    $rootScope.CartList.items = [{ Product: product, Count: 1 }];
                else
                $rootScope.CartList.items.push({
                    Product: product,
                    Count: 1
                });
            } else {
                $rootScope.CartList.items[id].Count++;
            }
            $rootScope.CartList.price += product.Price;
        },
        startOrder: function (userName, phone) {
            var order = {
                UserName: userName,
                Phone: phone,
                Products: new Array()
            };
            for (var i = 0; i < $rootScope.CartList.items.length; i++) {
                order.Products.push({
                    ProductId: $rootScope.CartList.items[i].Product.Id,
                    Count: $rootScope.CartList.items[i].Count
                });
            }
            //clear();
            this.clear();
            return webService.startOrder(order);          
        },
        remove: function (index) {
            $rootScope.CartList.price -= $rootScope.CartList.items[index].Product.Price * $rootScope.CartList.items[index].Count;
            $rootScope.CartList.items.splice(index, 1);
        },
        clear: function() {
            $rootScope.CartList.price = 0;
            $rootScope.CartList.items.length = 0;
        },
        price: function() {
            return CartList.price;
        },
        downCount: function(index) {           
            if ($rootScope.CartList.items[index].Count == 1) this.remove(index);
            else {
                $rootScope.CartList.price -= $rootScope.CartList.items[index].Product.Price;
                $rootScope.CartList.items[index].Count--;
            }
        },
        upCount: function(index) {
            $rootScope.CartList.price += $rootScope.CartList.items[index].Product.Price;
            $rootScope.CartList.items[index].Count++;
        }
    };

}).factory('compareService', function ($q, $rootScope, webService) {
    if (angular.isUndefined($rootScope.CompareList))
        $rootScope.CompareList = [];
    var compareList = $rootScope.CompareList;
    return {
        add: function (product) {
            var addToList = true;
            for (var i = 0; i < $rootScope.CompareList.length; i++)
                if ($rootScope.CompareList[i].Id == product.Id) addToList = false;
            if (addToList) compareList.push(product);
        },
        compare: function() {
            var indexes = [];
            for (var i = 0; i < compareList.length; i++)
                indexes.push(compareList[i].Id);
            return webService.qpost('api/Compare', indexes);
        },
        delete: function(index, data) {
            $rootScope.CompareList.splice(index, 1);
            for (var i = 0; i < data.length; i++)
                data[i].Values.splice(index, 1);
        }
    };
});

