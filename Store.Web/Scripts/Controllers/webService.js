﻿//if (angular.isUndefined(storeApp)) var storeApp = angular.module('storeApp', ["ngRoute"]);

angular.module('storeApp').factory('webService', function ($http, $q) {
    //Requster with promise object
    function request(url) {
        var deferrer = $q.defer();
        $http.get(url)
            .success(function (data, status, headers, config) {
                deferrer.resolve(data);
            }).error(function (data, status, headers, config) {
                var msg = "Unknown network error!";
                if (status == 401) msg = "You must authorize";
                deferrer.reject(msg);
            });
        return deferrer.promise;
    }
    function postRequest(url, body) {
        var deferrer = $q.defer();
        $http.post(url, body)
            .success(function (data, status, headers, config) {
                deferrer.resolve(data);
            })
            .error(function (data, status, headers, config) {
                var msg = "Unknown network error!";
                if (status == 401) msg = "You must authorize";
                deferrer.reject(msg);
            });
        return deferrer.promise;
    }
    function putRequest(url, body) {
        var deferrer = $q.defer();
        $http.put(url, body)
            .success(function (data, status, headers, config) {
                deferrer.resolve(data);
            })
            .error(function (data, status, headers, config) {
                var msg = "Unknown network error!";
                if (status == 401) msg = "You must authorize";
                deferrer.reject(msg);
            });
        return deferrer.promise;
    }
    function deleteRequest(url) {
        var deferrer = $q.defer();
        $http.delete(url)
            .success(function (data, status, headers, config) {
                deferrer.resolve(data);
            })
            .error(function (data, status, headers, config) {
                var msg = "Unknown network error!";
                if (status == 401) msg = "You must authorize";
                deferrer.reject(msg);
            });
        return deferrer.promise;
    }
    return {
        getData: function () {
            return request("api/Department");
        },

        getProducers: function (listIndex, page) {
            if (angular.isUndefined(page)) page = 1;
            return request("api/Department/" + page + "?List=" + listIndex);
        },

        getProducts: function (listIndex, producerIndex, page) {
            if (angular.isUndefined(page)) page = 1;
            var requestString = "api/Department/" + page + "?List=" + listIndex;
            if (producerIndex > 0) requestString += "&Producer=" + producerIndex;
            return request(requestString);
        },

        getProductList: function (listId, producerId) {
            var requestString = "api/List/" + listId;
            if (producerId > 0) requestString += "?producer=" + producerId;
            return request(requestString);
        },
        getSearchList: function (text, producerId) {
            //alert('asdfasdf');
            return postRequest("api/Search", {Id: producerId, Text: text});
        },
        getProductDetails: function (productId) {
            return request("api/Product/" + productId);
        },

        startOrder: function (data) {
            return postRequest('api/Cart/', data);
        },

        test: function () {
            return request("api/Cart");
        },

        qget: function (url) {
            return request(url);
        },
        qpost: function (url, data) {
            return postRequest(url, data);
        },
        qdel: function (url, data) {
            return deleteRequest(url, data);
        },
        qput: function (url, data) {
            return putRequest(url, data);
        }
    }
});

