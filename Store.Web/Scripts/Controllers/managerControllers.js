﻿//if (angular.isUndefined(storeApp)) var storeApp = angular.module('storeApp', ["ngRoute"]);

function TemplateScope($scope, service, map, id) {
    var promiseObj;
    function fillItem(source) {
        var result = {};
        if (angular.isUndefined(source)) {
            for (var i = 1; i < map.length; i++)
                result[map[i].key] = $scope[map[i].scope];
        } else {
            for (var i = 0; i < map.length; i++)
                result[map[i].key] = source[map[i].key];
        }
        return result;
    }

    $scope.afterRefresh = function () { };

    $scope.refresh = function () {
        promiseObj = service.getSets(id);
        promiseObj.then(function (value) {
            $scope.items = value;
            for (var i = 1; i < map.length; i++)
                $scope[map[i].scope] = "";
        });
        $scope.afterRefresh();
    }
    $scope.startEdit = function (index) {
        for (var i = 0; i < $scope.items.length; i++)
            if (i == index) $scope.items[i].editMode = true;
            else if ($scope.items[i].editMode) $scope.items[i].editMode = false;
        $scope.cencelItem = angular.copy($scope.items[index]);
    }

    $scope.cancelEdit = function (index) {
        $scope.items[index] = $scope.cencelItem;
        $scope.items[index].editMode = false;
    }

    $scope.save = function (index) {
        var department = fillItem($scope.items[index], true);
        promiseObj = service.saveSet(department);
        promiseObj.then(function () {
            $scope.refresh();
        }, function (msg) {
            alert(msg);
        });
    }

    $scope.create = function () {
        var department = fillItem();
        promiseObj = service.saveSet(department);
        promiseObj.then(function () {
            $scope.refresh();
            $scope.name = "";
            $scope.sort = "";
        }, function (msg) {
            alert(msg);
        });
    };
    $scope.delete = function (index) {
        promiseObj = service.deleteItem($scope.items[index].Id);
        promiseObj.then(function () {
            $scope.items.splice(index, 1);
        }, function (msg) {
            alert(msg);
        });
    }
}
//rename lists to sets to not be confused in list and listS controllers and services
angular.module('storeApp').controller("setController", function ($scope, setService) {
    TemplateScope($scope, setService, [
        { key: "Id", scope: "" },
        { key: "Name", scope: "name" },
        { key: "SortIndex", scope: "sort" }
    ]);
    $scope.refresh();
})

.controller("producerController", function ($scope, producerService) {
    TemplateScope($scope, producerService, [
        { key: "Id", scope: "" },
        { key: "Name", scope: "name" },
        { key: "Annotation", scope: "annotation" },
        { key: "Logo", scope: "logo" }
    ]);
    $scope.refresh();
})

.controller("detailsController", function ($scope, detailsService) {
    TemplateScope($scope, detailsService, [
        { key: "Id", scope: "" },
        { key: "Name", scope: "name" }
    ]);
    $scope.refresh();
})

.controller('imgController',
// ReSharper disable once InconsistentNaming
 function ($scope, FileUploader, $routeParams, webService) {
     var prodId = $routeParams.itemId;
     var uploader = $scope.uploader = new FileUploader({
         url: 'api/File/' + prodId
     });
     var promiseObj = webService.qget('api/File/' + prodId + '?list=true');
     promiseObj.then(function (data) {
         $scope.picturesIds = data;
     });

     uploader.onSuccessItem = function (fileItem, response, status, headers) {
         var id = Number($.parseXML(response).firstChild.textContent);
         $scope.picturesIds.push(id);
         fileItem.removeAfterUpload = true;
     };

     $scope.delete = function (index) {
         promiseObj = webService.qdel('api/File/' + $scope.picturesIds[index]);
         promiseObj.then(function () {
             $scope.picturesIds.splice(index, 1);
         });
     }

 }).controller('orderController', function ($scope, $routeParams, webService) {
     var promiseObj = webService.qget('api/Cart/');
     promiseObj.then(function (value) {
         $scope.list = value;
     });

     $scope.manage = function (index) {
         $scope.list[index].Status = 'Progress';
         $scope.save(index);
     };

     $scope.done = function (index) {
         $scope.list[index].Status = 'Done';
         $scope.save(index);
     };

     $scope.save = function(index) {
         promiseObj = webService.qput('api/Cart', $scope.list[index]);
         promiseObj.then(function() {},function() {
             $scope.list[index].Status = 'New';
         });
     }
     $scope.delete = function(index) {
         promiseObj = webService.qdel('api/Cart/' + $scope.list[index].Id);
         promiseObj.then(function() {
             $scope.list.splice(index, 1);
         }, function () {
             alert("Error. Try later.");
         });
     }
    $scope.sum = function(index) {
        var _sum = 0;
        for (var i = 0; i < $scope.list[index].Products.length; i++) {
            _sum += $scope.list[index].Products[i].Product.Price * $scope.list[index].Products[i].Count;
        }
        return _sum;
    };
});