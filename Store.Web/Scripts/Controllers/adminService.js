﻿//if (angular.isUndefined(storeApp)) var storeApp = angular.module('storeApp', ["ngRoute"]);

angular.module('storeApp').factory('adminService', function ($http, $q, webService) {
    return {
        getUsers: function() {
            return webService.qget("api/Users");
        },
        addUser: function(userName, password) {
            return webService.qpost("api/Users", { Name: userName, Password: password });
        },
        deleteUser: function(userName) {
            return webService.qdel("api/Users/?userName=" + userName, "");
        },
        addRole: function(userName, role) {
            return webService.qput("api/Users", { "Name": userName, "Role": role });
        },
        deleteUserFromRole: function (userName, role) {
            return webService.qdel("api/Users?userName=" + userName + "&role=" + role);
        }
    };
});