﻿angular.module('storeApp').config(function ($routeProvider) {

    $routeProvider.when('/', { redirectTo: '/list/1' });


    $routeProvider.when('/test/',
    {
        templateUrl: 'Template/Test',
        controller: "testController"
    });


        $routeProvider.when('/list/:listId/:producerId',
        {
            templateUrl: 'Template/List',
            controller: "listController"
        });
        $routeProvider.when('/list/:listId',
        {
            templateUrl: 'Template/List',
            controller: "listController"
        });
        $routeProvider.when('/search/:text/:producerId',
            {
                templateUrl: 'Template/List',
                controller: "listController"
            });
        $routeProvider.when('/item/:itemId',
        {
            templateUrl: 'Template/Item'
        });
        $routeProvider.when('/cart/',
        {
            templateUrl: 'Template/Cart',
            controller: 'cartController'
        });
        $routeProvider.when('/compare/',
            {
                templateUrl: 'Template/Compare',
                controller: 'compareController'
            });
        $routeProvider.when('/success/',
            {
                templateUrl: 'Template/Success'
            });
        $routeProvider.when('/users/',
        {
            templateUrl: 'Template/Users',
            controller: 'usersController'
        });
        $routeProvider.when('/users/',
        {
            templateUrl: 'Template/Users',
            controller: 'usersController'
        });
        $routeProvider.when('/lists/',
        {
            templateUrl: 'Template/Lists',
            controller: 'setController'
        });
        $routeProvider.when('/newProduct/',
            {
                templateUrl: 'Template/NewProduct',
                controller: 'newProductController'
            });
        $routeProvider.when('/order/',
            {
                templateUrl: 'Template/Order',
                controller: 'orderController'
            });
        $routeProvider.when('/producers/',
        {
            templateUrl: 'Template/Producers',
            controller: 'producerController'
        });
        $routeProvider.when('/details/',
            {
                templateUrl: 'Template/Details',
                controller: 'detailsController'
            });
        $routeProvider.when('/news/',
                {
                    templateUrl: 'Template/News'
                });
        $routeProvider.when('/help/',
                {
                    templateUrl: 'Template/Help'
                });
        $routeProvider.when('/about/',
                {
                    templateUrl: 'Template/About'
                });
        
        $routeProvider.otherwise({ redirectTo: '/list/1' });
    });