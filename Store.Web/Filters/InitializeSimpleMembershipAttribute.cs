﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Web.Mvc;
using WebMatrix.WebData;
using Login.Delete.Models;
using System.Web.Security;

namespace Login.Delete.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Обеспечение однократной инициализации ASP.NET Simple Membership при каждом запуске приложения
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        public class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<UsersContext>(null);

                try
                {
                    using (var context = new UsersContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Создание базы данных SimpleMembership без схемы миграции Entity Framework
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                        if (!WebSecurity.Initialized)
                            WebSecurity.InitializeDatabaseConnection("AuthContainer", "UserProfile", "UserId", "UserName", autoCreateTables: true);
                       // WebSecurity.InitializeDatabaseConnection();

                        if (!Roles.RoleExists("Administrator"))
                        {
                            Roles.CreateRole("Administrator");
                        }

                        if (!Roles.RoleExists("Manager"))
                        {
                            Roles.CreateRole("Manager");
                        }

                        if (Membership.GetUser("Admin", false) == null)
                        {
                            WebSecurity.CreateUserAndAccount("Admin", "Admin");
                            Roles.AddUserToRole("Admin", "Administrator");
                        }
                    
                    }

                    //WebSecurity.InitializeDatabaseConnection("AuthContainer", "UserProfile", "UserId", "UserName", autoCreateTables: true);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("Не удалось инициализировать базу данных ASP.NET Simple Membership. Чтобы получить дополнительные сведения, перейдите по адресу: http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }
}
