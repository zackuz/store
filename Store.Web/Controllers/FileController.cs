﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Store.Services.Models;
using Store.Services.Services;
using System.Web.Mvc;
using System.IO;
using System.Net.Http.Headers;

namespace Store.Web.Controllers
{
    public class FileController : ApiController
    {
        private readonly PictureService _service = new PictureService();
        // GET api/file
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


         //GET api/file/5
        public HttpResponseMessage Get(int id)
        {

            Picture picture = _service.GetPicture(id);

            if (picture == null) return Request.CreateResponse(HttpStatusCode.InternalServerError, "No picture");

            var httpResponseMessage = new HttpResponseMessage();
            var memoryStream = new MemoryStream();
            memoryStream.Write(picture.FileData, 0, picture.FileData.Length);


            httpResponseMessage.Content = new ByteArrayContent(picture.FileData);

            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(picture.Mime);

            httpResponseMessage.StatusCode = HttpStatusCode.OK;

            return httpResponseMessage;
        }


        public int[] Get(int id, bool list)
        {
            return _service.GetIds(id);
        }

        // POST api/file
        public HttpResponseMessage Post(int id)
        {

            var httpRequest = HttpContext.Current.Request;
            HttpResponseMessage httpResponse;
            if (httpRequest.Files.Count > 0)
            {
                var file = httpRequest.Files[0];
                var picture = new Picture
                {
                    ProductId = id,
                    Mime = file.ContentType,
                    FileData = new byte[file.ContentLength]
                };
                file.InputStream.Read(picture.FileData, 0, file.ContentLength);
                //_service.Create(picture);
                return Request.CreateResponse(HttpStatusCode.OK, _service.Create(picture));
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
        }

        // DELETE api/file/5
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
