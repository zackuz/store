﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Store.Services.Models;
using Store.Services.Services;

namespace Store.Web.Controllers
{
    public class ListController : ApiController
    {
        private readonly ProductsService _service = new ProductsService();
        private readonly DepartmentService _departmentService = new DepartmentService();

        // GET api/list
        public Product[] Get()
        {
            return Get(_departmentService.FirstId);
        }

        // GET api/list/5
        public Product[] Get(int id)
        {
            return _service.GetProducts(id);
        }

        public Product[] Get(int id, int producer)
        {
            return _service.GetProducts(id, producer);
        }

        // POST api/list
        public void Post([FromBody]string value)
        {
        }

        // PUT api/list/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/list/5
        public void Delete(int id)
        {
        }
    }
}
