﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Login.Delete.Filters;

namespace Store.Web.Controllers
{
    [InitializeSimpleMembership]
    public class TemplateController : Controller
    {
        //
        // GET: /Template/
        public ActionResult Test()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List()
        {
            return PartialView();
        }
        
        public PartialViewResult Item()
        {
            return PartialView();
        }

        public PartialViewResult Cart()
        {
            return PartialView();
        }
        public PartialViewResult Compare()
        {
            return PartialView();
        }

        public PartialViewResult Success()
        {
            return PartialView();
        }

        [Authorize(Roles = "Administrator")]
        public PartialViewResult Users()
        {
            return PartialView();
        }

        [Authorize(Roles = "Manager")]
        public PartialViewResult Lists()
        {
            return PartialView();
        }
        [Authorize(Roles = "Manager")]
        public PartialViewResult Producers()
        {
            return PartialView();
        }
        [Authorize(Roles = "Manager")]
        public PartialViewResult Details()
        {
            return PartialView();
        }
        [Authorize(Roles = "Manager")]
        public PartialViewResult NewProduct()
        {
            return PartialView();
        }
        [Authorize(Roles = "Manager")]
        public PartialViewResult Order()
        {
            return PartialView();
        }

        //Information views
        public PartialViewResult News()
        {
            return PartialView();
        }

        public PartialViewResult Help()
        {
            return PartialView();
        }

        public PartialViewResult About()
        {
            return PartialView();
        }

    }
}
