﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Store.Services.Models;
using Store.Services.Services;

namespace Store.Web.Controllers
{
    public class ProductController : ApiController
    {
        private readonly ProductsService _service = new ProductsService();
        // GET api/product
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/product/5
        public FullProductInformation Get(int id)
        {
            return _service.GetProduct(id);
        }

        public ProductPost Get(int id, string mode)
        {
            return _service.GetPostProduct(id);
        }
        // POST api/product
        //public void Post([FromBody]Product value)
        //{
        //    var i = value;
        //}

        public HttpResponseMessage Post([FromBody]ProductPost value)
        {
            int id =_service.CreateOrEdit(value);
            if (id < 0) return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error!!");

            return Request.CreateResponse(HttpStatusCode.OK, id);
        }

        // PUT api/product/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/product/5
        public void Delete(int id)
        {
        }
    }
}
