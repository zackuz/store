﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Store.Services.Models;
using Store.Services.Services;

namespace Store.Web.Controllers
{
    [Authorize(Roles = "Manager")]
    public class SetController : ApiController
    {
        // GET api/set
        //rename lists to sets to not be confused in list and listS controllers and services

        private readonly DepartmentService _service = new DepartmentService();
        public IEnumerable<SortedDepartment> Get()
        {
            return _service.GetDepartments();
        }

        // GET api/set/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/set
        public void Post([FromBody]SortedDepartment value)
        {
            _service.CreateOrEdit(value);
        }

        // PUT api/set/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/set/5
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
