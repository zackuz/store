﻿using Store.Services.Models;
using Store.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Store.Web.Controllers
{
    [Authorize(Roles = "Manager")]
    public class ProducerController : ApiController
    {
        private readonly ProducerService _service = new ProducerService();
        // GET api/producer
        public IEnumerable<FullProducer> Get()
        {
            return _service.GetProducers();
        }

        // GET api/producer/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/producer
        public void Post([FromBody]FullProducer value)
        {
            _service.CreateOrEdit(value);
        }

        // PUT api/producer/5
        [AllowAnonymous]
        public void Put(int id, [FromBody]string value)
        {
            var i = value;
            i += ")";
        }

        // DELETE api/producer/5
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
