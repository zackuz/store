﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI;
using Store.Services.Services;
using Store.Services.Models;

namespace Store.Web.Controllers
{
    public class DepartmentController : ApiController
    {
        readonly DataServices _services = new DataServices();
        // GET api/department
        public JsonAnswer Get()
        {
            return _services.GetAnswer();
        }

        // Lists for productId
        public IEnumerable<ProductDepartment> Get(int id)
        {
            return _services.GetDepartments(id);
        }

        public JsonAnswer Get(int id, int list)
        {
            return _services.GetAnswer(id, list);
        }

        public JsonAnswer Get(int id, int list, int producer)
        {
            return _services.GetAnswer(id, list, producer);
        }
        // POST api/department
        public void Post([FromBody]string value)
        {
        }

        // PUT api/department/5
        public void Put(int id, [FromBody]ProductDepartment[] value)
        {
            _services.SetDepartments(id, value);
        }

        // DELETE api/department/5
        public void Delete(int id)
        {
        }
    }
}
