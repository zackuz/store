﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using Store.Services.Services;
using Store.Services.Models;

namespace Store.Web.Controllers
{
    public class CartController : ApiController
    {
        private readonly OrderService _service = new OrderService();
        // GET api/cart
        [Authorize(Roles = "Manager")]
        public IEnumerable<Order> Get()
        {
            return _service.GetOrders();
        }

        // GET api/cart/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/cart
        [AllowAnonymous]
        public void Post([FromBody]Order newOrder)
        {
            _service.StartOrder(newOrder);
        }

        // PUT api/cart/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Authorize(Roles = "Manager")]
        public void Put(Order order)
        {
            _service.Save(order);
        }
        // DELETE api/cart/5
        public void Delete(int id)
        {
        }
    }
}
