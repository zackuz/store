﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Store.Services.Models;
using Store.Services.Services;

namespace Store.Web.Controllers
{
    public class ProductDetailController : ApiController
    {
        private readonly ProductDetailService _service = new ProductDetailService();
        public IEnumerable<ProductDetail> Get(int id)
        {
            return _service.GetProductDetails(id);
        }

        // POST api/productdetail
        public void Post([FromBody]ProductDetail value)
        {
            _service.Edit(value);
        }

        // PUT api/productdetail/5
        public void Put(int id, [FromBody]ProductDetailInt value)
        {
            _service.Create(id, value);
        }

        // DELETE api/productdetail/5
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
