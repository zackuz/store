﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Store.Services.Models;
using Store.Services.Services;

namespace Store.Web.Controllers
{
    public class DetailController : ApiController
    {
        private readonly DetailService _service = new DetailService();
        // GET api/detail
        public IEnumerable<Detail> Get()
        {
            return _service.GetDetail();
        }

        // GET api/detail/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/detail
        public void Post([FromBody]Detail value)
        {
            _service.CreateOrEdit(value);
        }

        // PUT api/detail/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/detail/5
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
