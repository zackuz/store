﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using Login.Delete.Filters;
using Login.Delete.Models;
using WebMatrix.WebData;

namespace Store.Web.Controllers
{
    [InitializeSimpleMembership]
    [Authorize(Roles = "Administrator")]
    public class UsersController : ApiController
    {
        // GET api/users
        public object Get()
        {
            var context = new UsersContext();
            var users = context.UserProfiles.Select(x => x.UserName).ToArray();
            var userList = new List<UserItem>();
            foreach (var item in users)
            {
                var user = Membership.GetUser(item);

                userList.Add(new UserItem
                {
                    Id = Convert.ToInt32(user.ProviderUserKey),
                    Name = user.UserName,
                    Online = user.IsOnline,
                    Roles = Roles.GetRolesForUser(user.UserName)
                });
            }

            return new
            {
                Users = userList,
                Roles = Roles.GetAllRoles()
            };
        }

        // GET api/users/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/users
        public HttpResponseMessage Post([FromBody]NewUser value)
        {
            HttpResponseMessage response; 
            try
            {
                WebSecurity.CreateUserAndAccount(value.Name, value.Password);
                var user = Membership.GetUser(value.Name);
                response = Request.CreateResponse(HttpStatusCode.OK, new UserItem
                {
                    Id = Convert.ToInt32(user.ProviderUserKey),
                    Name = user.UserName,
                    Online = user.IsOnline,
                    Roles = Roles.GetRolesForUser(user.UserName)
                });
            }
            catch (Exception)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }

            return response;
        }

        // PUT api/users/5
        public void Put(int id, [FromBody]string value)
        {
        }

        public HttpResponseMessage Put([FromBody]IDictionary<string, string> value)
        {
            HttpResponseMessage response;
            try
            {
                Roles.AddUserToRole(value["Name"], value["Role"]);
                response = Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }
            return response;
        }
        // DELETE api/users/5
        public HttpResponseMessage Delete(string userName)
        {
            try
            {
                Roles.RemoveUserFromRoles(userName, Roles.GetRolesForUser(userName));
                Membership.DeleteUser(userName);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }
        public HttpResponseMessage Delete(string userName, string role)
        {
            HttpResponseMessage response;
            try
            {
                Roles.RemoveUserFromRole(userName, role);
                response = Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }
            return response;
        }
    }

    class UserItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Online { get; set; }
        public string[] Roles { get; set; }
    }

    public class NewUser
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
