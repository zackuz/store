﻿using System.Data.Entity;
using System.Dynamic;
using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Picture = Store.Services.Models.Picture;

namespace Store.Services.Services
{
    public class PictureService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;

        public int GetFirstId(int productId)
        {
            lock (_context)
            {
                var pic = _context.Pictures.SingleOrDefault(x => x.ProductId == productId);
                if (pic != null) return pic.Id;
                return 0;
            }
        }
        public int Create(Picture picture)
        {
            var Picture = new Data.Models.Picture
            {
                Data = picture.FileData,
                Mime = picture.Mime,
                ProductId = picture.ProductId
            };
            lock (_context)
            {
                _context.Pictures.Add(Picture);
                _context.SaveChanges();
            }
            return Picture.Id;
        }

        public Picture GetPicture(int id)
        {
            lock (_context)
            {
                var picture = _context.Pictures.SingleOrDefault(x => x.Id == id);
                if (picture == null) return null;
                return new Picture
                {
                    Id = picture.Id,
                    ProductId = picture.ProductId,
                    Mime = picture.Mime,
                    FileData = picture.Data
                };
            }
        }

        public int[] GetIds(int productId)
        {
            lock (_context)
            {
                return _context.Pictures.Where(x => x.ProductId == productId)
                    .Select(x => x.Id).ToArray();
            }
        }

        public void Delete(int id)
        {
            lock (_context)
            {
                _context.Pictures.Remove(_context.Pictures.Find(id));
                _context.SaveChanges();
            }
        }
    }
}
