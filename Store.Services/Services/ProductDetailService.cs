﻿using System.Data.Entity;
using Store.Data.Models;
using Store.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Services
{
    public class ProductDetailService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;

        public ProductDetail[] GetProductDetails(int productId)
        {
            lock (_context) { 
            return _context.ProductDetails
                .Include(x => x.Details)
                .Where(x => x.ProductId == productId)
                .Select(x => new ProductDetail
                {
                    Id = x.Id,
                    Name = x.Details.Name,
                    Value = x.Text
                }).ToArray();
            }
        }

        public void Edit(ProductDetail value)
        {
            _context.ProductDetails.Find(value.Id).Text = value.Value;
            _context.SaveChanges();
        }

        public void Create(int id, ProductDetailInt value)
        {
            _context.ProductDetails.Add(new ProductDetails
            {
                ProductId = id,
                DetailsId = value.DetailId,
                Text = value.Value
            });
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.ProductDetails.Remove(
                _context.ProductDetails.Find(id));
            _context.SaveChanges();
        }
    }

}
