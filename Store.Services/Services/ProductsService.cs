﻿using System.Data.Entity;
using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServModels = Store.Services.Models;

namespace Store.Services.Services
{
    public class ProductsService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;
//        private readonly PictureService _picService = new PictureService();

        public ServModels.Product[] GetProducts()
        {
            lock (_context)
            {
                return _context.Products.Include(e => e.Producer).Include(e => e.Picture)
                    .Select(x => new ServModels.Product
                    {
                        Id = x.Id,
                        PictureId = x.Picture.Select(m => m.Id).FirstOrDefault(),
                        Name = x.Name,
                        Model = x.Model,
                        Producer = x.Producer.Name,
                        Annotation = x.Annotation,
                        Price = x.Price,
                        InStock = x.Price > 0
                    }).ToArray();
            }
        }

        public ServModels.Product[] GetProducts(int department)
        {
            lock (_context)
            {
                return _context.Products.Include(e => e.Producer).Where(c => c.Department.Any(d => d.Id == department))
                    .Select(x => new ServModels.Product
                    {
                        Id = x.Id,
                        PictureId = x.Picture.Select(m => m.Id).FirstOrDefault(),
                        Name = x.Name,
                        Model = x.Model,
                        Producer = x.Producer.Name,
                        Annotation = x.Annotation,
                        Price = x.Price,
                        InStock = x.Price > 0
                    }).ToArray();
            }
        }

        public ServModels.Product[] GetProducts(int department, int producer)
        {
            lock (_context)
            {
                return
                    _context.Products.Include(e => e.Producer)
                        .Where(c => c.Department.Any(d => d.Id == department) && c.ProducerId == producer)
                        .Select(x => new ServModels.Product
                        {
                            Id = x.Id,
                            PictureId = x.Picture.Select(m => m.Id).FirstOrDefault(),
                            Name = x.Name,
                            Model = x.Model,
                            Producer = x.Producer.Name,
                            Annotation = x.Annotation,
                            Price = x.Price,
                            InStock = x.Price > 0
                        }).ToArray();
            }
        }

        public ServModels.FullProductInformation GetProduct(int productId)
        {
            lock (_context)
            {
                var product = _context.Products
                    .Include(e => e.Producer)
                    .Include(e => e.ProductDetails)
                    .Include(e => e.Comments)
                    .Single(c => c.Id == productId);
                var fullProductDateil = new ServModels.FullProductInformation()
                {
                    Id = product.Id,
                    Name = product.Name,
                    Model = product.Model,
                    Producer = product.Producer.Name,
                    Annotation = product.Annotation,
                    Price = product.Price,
                    InStock = product.Price > 0,

                    Details = product.ProductDetails.Select(x => new ServModels.ProductDetail
                    {
                        Id = x.Id,
                        Name = x.Details.Name,
                        Value = x.Text
                    }).ToArray(),

                    Comments = product.Comments
                        .Where(c => c.ProductId == productId)
                        .Select(x => new ServModels.Comment
                        {
                            User = x.UserName,
                            Text = x.Text,
                            DateTime = x.DateTime,
                            Mark = x.Mark
                        }).ToArray(),

                    PicturesIds = product.Picture
                        .Where(c => c.ProductId == productId)
                        .Select(x => x.Id).ToArray()
                };
                return fullProductDateil;
            }
        }

        public ServModels.ProductPost GetPostProduct(int id)
        {
            //ServModels.ProductPost prod = new ServModels.ProductPost();
            lock (_context){
            return _context.Products.Include(e => e.Producer).Include(e => e.Picture)
                .Select(x => new ServModels.ProductPost
                {
                    Id = x.Id,
                    PictureId = x.Picture.Select(m => m.Id).FirstOrDefault(),
                    Name = x.Name,
                    Model = x.Model,
                    Producer = x.Producer.Name,
                    ProducerId = x.ProducerId,
                    Annotation = x.Annotation,
                    Price = x.Price,
                    InStock = x.Price > 0,
                    Count = x.Count
                }).Single(c => c.Id == id);
            }
        }

        public int CreateOrEdit(ServModels.ProductPost product)
        {
            var item = new Product();
            lock (_context)
            {
                if (product.Id < 1)
                {
                    item.Name = product.Name;
                    item.Model = product.Model;
                    item.Annotation = product.Annotation;
                    item.ProducerId = product.ProducerId;
//                    item.Producer = _context.Producers.Find(product.ProducerId);
                    item.Price = product.Price;
                    item.Count = product.Count;
                    _context.Products.Add(item);
                }
                else //edit
                {
                    item = _context.Products.Single(x => x.Id == product.Id);
                    item.Name = product.Name;
                    item.Model = product.Model;
                    item.Annotation = product.Annotation;
                    item.ProducerId = product.ProducerId;
                    item.Price = product.Price;
                    item.Count = product.Count;
                }
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception)
                { return 0; }
                
                return item.Id;
            }
        }
    }
}
