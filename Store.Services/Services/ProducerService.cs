﻿using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Services.Models;
using Producer = Store.Services.Models.Producer;

namespace Store.Services.Services
{
    public class ProducerService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;
       
        public IEnumerable<FullProducer> GetProducers()
        {
            lock (_context)
            {
                return _context.Producers.Select(x => new FullProducer
                {
                    Id = x.Id,
                    Name = x.Name,
                    Annotation = x.Annotation,
                    Logo = x.Logo
                }).ToArray();
            }
        }

        

        public void CreateOrEdit(FullProducer producer)
        {
            if (producer.Id < 1)//Create
            {
                _context.Producers.Add(new Store.Data.Models.Producer
                {
                    Id = producer.Id,
                    Name = producer.Name,
                    Annotation = producer.Annotation,
                    Logo = producer.Logo
                });
            }
            else//edit
            {
                var item = _context.Producers.Single(x => x.Id == producer.Id);
                item.Name = producer.Name;
                item.Annotation = producer.Annotation;
                item.Logo = producer.Logo;
            }
            _context.SaveChanges();
        }


        public void Delete(int id)
        {
            var item = _context.Producers.Find(id);
            _context.Producers.Remove(item);
            _context.SaveChanges();
        }
    }
}
