﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Store.Data.Models;
using ServModels = Store.Services.Models;

namespace Store.Services.Services
{
    public class OrderService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;
        public void StartOrder(ServModels.Order order)
        {
            var contextOrder = new Order
            {
                Customer = order.UserName,
                Phone = order.Phone
            };
            _context.Orders.Add(contextOrder);
            _context.SaveChanges();
            foreach (var item in order.Products)
            {
                contextOrder.OrderItem.Add(new OrderItem
                {
                    Count = item.Count,
                    ProductId = item.ProductId
                });
            }
           
            _context.SaveChanges();
        }

        public ServModels.Order[] GetOrders()
        {
            lock (_context)
            {
                return _context.Orders
                    .Include(q=>q.OrderItem)
                    .Where(x => x.Status != OrderStatus.Done)
                    .Select(m => new ServModels.Order
                    {
                        Id = m.Id,
                        UserName = m.Customer,
                        Phone = m.Phone,
                        Annotation = m.Annotation,
                        Status = m.Status.ToString(),
                        Products = m.OrderItem.Select(o => new ServModels.OrderItem
                        {
                            ProductId = o.ProductId,
                            Count = o.Count,
                            Product = new ServModels.Product
                            {
                                Id = o.Product.Id,
                                Name = o.Product.Name,
                                Annotation = o.Product.Annotation,
                                Model = o.Product.Model,
                                Price = o.Product.Price,
                                Producer = o.Product.Producer.Name,
                                InStock = o.Product.Count > 0
                            }
                        }).ToList()
                    }).ToArray();
            }
        }


        public void Save(ServModels.Order order)
        {
            lock(_context)
            {
                var conOrder = _context.Orders.Find(order.Id);
                OrderStatus status;
                if (Enum.TryParse(order.Status, out status)) conOrder.Status = status;
                else conOrder.Status = null;
                conOrder.Annotation = order.Annotation;
                if (conOrder.Status == OrderStatus.Done)
                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            foreach (var product in order.Products)
                            {
                                var bufProduct = _context.Products.Find(product.ProductId);
                                //Add some test
                                bufProduct.Count -= product.Count;
                            }

                            _context.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                        }
                    }


                _context.SaveChanges();
            }
        }
    }

}
