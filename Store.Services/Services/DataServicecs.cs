﻿using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Services.Helpers;
using Store.Services.Models;
using Department = Store.Services.Models.Department;

namespace Store.Services.Services
{
    public class DataServices
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;
        private readonly ProductsService _productService = new ProductsService();

        public JsonAnswer GetAnswer()
        {
            lock (_context)
            {
                var item = _context.Departments.First();
                int list = (item == null) ? 1 : item.Id;
                return new JsonAnswer
                {
                    Departments = _context.Departments
                        .OrderBy(o => o.SortValue)
                        .Select(x => new Store.Services.Models.Department
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToArray(),
                    Producers = GetProdusers(list),
                    Products = _productService.GetProducts(list)

                };
            }
        }

        public JsonAnswer GetAnswer(int page, int list)
        {
            return new JsonAnswer
            {
                Producers = GetProdusers(list),
                Products = _productService.GetProducts(list)
            };
        }

        public JsonAnswer GetAnswer(int page, int list, int producer)
        {
            return new JsonAnswer
            {
                Products = _productService.GetProducts(list, producer)
            };
        }

        private Store.Services.Models.Producer[] GetProdusers(int departmentId)
        {
            lock (_context)
            {
                return _context.Producers
                    .Where(x => x.Product.Any(p=> p.Department.Any(d=>d.Id==departmentId)))
                    .Select(x => new Store.Services.Models.Producer
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToArray();
            }
        }

        public IEnumerable<ProductDepartment> GetDepartments(int id)
        {
            lock (_context)
            {
                IEnumerable<int> list = _context.Products.Find(id).Department.Select(x => x.Id).ToArray();

                var result = _context.Departments.Select(item => new ProductDepartment
                {
                    Id = item.Id,
                    Name = item.Name,
                    Consist = list.Any(x => item.Id == x)
                }).ToArray();
                return result;
            }
        }

        public void SetDepartments(int productId, ProductDepartment[] value)
        {
            lock (_context)
            {
                var delList = new List<Store.Data.Models.Department>();
                Store.Data.Models.Product product = _context.Products.Include("Department").Single(x => x.Id == productId);
                foreach(var item in value)
                    if (item.Consist)
                    {
                        if(product.Department.All(x=>x.Id!=item.Id))
                            product.Department.Add(_context.Departments.Find(item.Id));
                    }
                    else
                    {
                        delList.AddRange(product.Department.Where(x => x.Id == item.Id));
                    }
                foreach (var item in delList)
                    product.Department.Remove(item);

                //foreach (var item in product.Department)
                //{
                //    if (value.All(x => x.Id != item.Id)) product.Department.Remove(item);
                //}
                //foreach (var item in value)
                //{
                //    if (product.Department.All(x=>x.Id != item.Id))
                //    {
                //        product.Department.Add(_context.Departments.Find(item.Id));
                //    }
                //}
                _context.SaveChanges();
            }
        }
    }
}
