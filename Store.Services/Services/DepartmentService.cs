﻿using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Services.Models;
using Department = Store.Data.Models.Department;

namespace Store.Services.Services
{
    public class DepartmentService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;
        private static int _firstId;

        public int FirstId
        {
            get
            {
                if (_firstId == 0) _firstId = _context.Departments.First().Id;
                return _firstId;
            }
        }
        public IEnumerable<SortedDepartment> GetDepartments()
        {
            lock (_context)
            {
                return _context.Departments.OrderBy(x => x.SortValue).Select(x => new SortedDepartment
                {
                    Id = x.Id,
                    Name = x.Name,
                    SortIndex = x.SortValue
                }).ToArray();
            }
        }

        public void CreateOrEdit(SortedDepartment department)
        {
            if (department.Id < 1)//Create
            {
                _context.Departments.Add(new Store.Data.Models.Department
                {
                    Id = department.Id,
                    Name = department.Name,
                    SortValue = department.SortIndex
                });
            }
            else//edit
            {
                var item = _context.Departments.Single(x => x.Id == department.Id);
                item.Name = department.Name;
                item.SortValue = department.SortIndex;
            }
            _context.SaveChanges();
        }


        public void Delete(int id)
        {
            var item = _context.Departments.Find(id);
            _context.Departments.Remove(item);
            _context.SaveChanges();
        }
    }
}
