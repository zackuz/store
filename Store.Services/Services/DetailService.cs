﻿using System.Data.Entity;
using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Services.Models;

namespace Store.Services.Services
{
    public class DetailService
    {
        private readonly EntitiesContainer _context = EntitiesContainer.Instance;

        public IEnumerable<Detail> GetDetail()
        {
            lock (_context)
            {
                return _context.Details.Select(x => new Detail
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToArray();
            }
        }

        public void CreateOrEdit(Detail detail)
        {
            if (detail.Id < 1)//Create
            {
                _context.Details.Add(new Store.Data.Models.Details
                {
                    Id = detail.Id,
                    Name = detail.Name
                });
            }
            else//edit
            {
                var item = _context.Details.Single(x => x.Id == detail.Id);
                item.Name = detail.Name;
            }
            _context.SaveChanges();
        }


        public void Delete(int id)
        {
            var item = _context.Details.Find(id);
            _context.Details.Remove(item);
            _context.SaveChanges();
        }
    }
}
