﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Product
    {
        public int Id { get; set; }
        public int PictureId { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string Annotation { get; set; }
        public string Producer { get; set; }
        public double Price { get; set; }
        public bool InStock { get; set; }
    }

    public class ProductPost : Product
    {
        public int ProducerId { get; set; }
        public int Count { get; set; }
    }
}
