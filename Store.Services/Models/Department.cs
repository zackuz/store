﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class SortedDepartment : Department
    {
        public int SortIndex { get; set; }
    }

    public class ProductDepartment : Department
    {
        public bool Consist { get; set; }
    }
}
