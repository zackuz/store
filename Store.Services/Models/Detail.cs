﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Detail
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
