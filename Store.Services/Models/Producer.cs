﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Producer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class FullProducer : Producer
    {
        public string Annotation { get; set; }
        public string Logo { get; set; }
    }
}
