﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class FullProductInformation : Product
    {
        public ProductDetail[] Details { get; set; }
        public int[] PicturesIds { get; set; }
        public Comment[] Comments { get; set; }
    }
}
