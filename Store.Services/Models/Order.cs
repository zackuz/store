﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public IEnumerable<OrderItem> Products { get; set; }
        public string Annotation { get; set; }
        public string Status { get; set; }

    }
}
