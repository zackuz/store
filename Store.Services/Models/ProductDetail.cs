﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class ProductDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ProductDetailInt
    {
        public int Id { get; set; }
        public int DetailId { get; set; }
        public string Value { get; set; }
    }
}
