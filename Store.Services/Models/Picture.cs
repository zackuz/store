﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Picture
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public byte[] FileData { get; set; }
        public string Mime { get; set; }

    }
}
