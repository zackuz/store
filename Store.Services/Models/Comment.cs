﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Services.Models
{
    public class Comment
    {
        public string Text { get; set; }
        public string User { get; set; }
        public DateTime DateTime { get; set; }
        public byte Mark { get; set; }
        public int ProductId { get; set; }
    }
}
