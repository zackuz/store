﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Services.Models;

namespace Store.Services.Helpers
{
    public static class ModelsConverter
    {
        public static Store.Services.Models.Department ToServiceModel(this Store.Data.Models.Department value)
        {
            return new Department
            {
                Id = value.Id,
                Name = value.Name
            };
        }

        public static Store.Services.Models.Producer ToServiceModel(this Store.Data.Models.Producer value)
        {
            return new Producer
            {
                Id = value.Id,
                Name = value.Name
            };
        }
    }
}
